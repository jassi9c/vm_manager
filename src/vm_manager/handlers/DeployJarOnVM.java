package vm_manager.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import prometheus.Cloud;
import vm_manager.wizards.CreateDNSServerWizard;
import vm_manager.wizards.DeployJarOnVmWizard;

public class DeployJarOnVM extends AbstractHandler{
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		
		IStructuredSelection structured= (IStructuredSelection)window.getSelectionService().getSelection("org.eclipse.jdt.ui.PackageExplorer");
		IFile file = (IFile) structured.getFirstElement();
		String filePath = file.getLocation().toPortableString();

		
		Cloud cloud = vm_manager.Activator.getCloud();
		if(cloud==null){
			MessageDialog.openInformation(window.getShell(), "Credentials file not set" , "Please set the credentials file from preferences before proceeding.");
			return null;
		}
		
		WizardDialog wizardDialog = new WizardDialog(window.getShell(), new DeployJarOnVmWizard(cloud,filePath));
		
		if (wizardDialog.open() == Window.OK) {
		} else {
		}		
		return null;
	}

}
