package vm_manager.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String CREDENTIALS_FILE_PATH = "credentialsFilePreference";
	
	public static final String SSH_KEYS_PATH = "sshFolderPreference";
	
	public static final String P_STRING = "stringPreference";
	
}
