package vm_manager.preferences;

import org.eclipse.jface.preference.*;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.IWorkbench;
import vm_manager.Activator;

public class VMManagerPreferences 	extends FieldEditorPreferencePage 	implements IWorkbenchPreferencePage {

	public static StringFieldEditor statusField;
	public VMManagerPreferences() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("Set the credentials file and ssh keys folder");
	}
	
	public void createFieldEditors() {
		addField(new FileFieldEditor(PreferenceConstants.CREDENTIALS_FILE_PATH,"&Credentials File:", getFieldEditorParent()));
		addField(new DirectoryFieldEditor(PreferenceConstants.SSH_KEYS_PATH,"&SSH Keys Folder:", getFieldEditorParent()));
			statusField =new StringFieldEditor(PreferenceConstants.P_STRING, "Connection status : ", getFieldEditorParent()); 
		statusField.setEnabled(false, getFieldEditorParent());
		addField(statusField);
		if(Activator.getDefault().getCloud()==null)
			Activator.getDefault().connectToCloudUsingCredentialsFile(Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.CREDENTIALS_FILE_PATH));
		statusField.setStringValue(Activator.getDefault().getCloud()==null?"Not connected":"Connected");
	}

	public void init(IWorkbench workbench) {
	}
	
}