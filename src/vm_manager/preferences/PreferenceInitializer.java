package vm_manager.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import vm_manager.Activator;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		/*
		IPreferenceStore store = vm_manager.Activator.getDefault().getPreferenceStore();
		if(store!=null){
			store.setDefault(PreferenceConstants.P_STRING,"Not Connected");
		}
		*/
	}

}
