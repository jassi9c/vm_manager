package vm_manager.views;

import java.util.Date;
import java.util.ArrayList;
import java.util.Map;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import prometheus.Cloud;
import prometheus.VirtualMachine;
import vm_manager.views.VMView.ViewContentProvider;

public class VMActivity extends ViewPart{

	public static final String ID = "vm_manager.views.VMView";

	private Cloud cloud;
	private TableViewer viewer;
	private static VMActivity def;
	
	private Action clear_activity;
	private Action save_activity;
	private Action see_json_request;
	private Action doubleClickActivity;
	private Action executeAgain;
	
	static ArrayList<Request> reqs = new ArrayList<Request>();;
	
	public static void addRequest(Request req){
		Date now = new Date();
		req.launchDateTime= now.toString();
		reqs.add(req);
		if(def!=null){
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					def.refresh();
				}
			});
		}
	}
	
	public static void updateRequest(Request req , Request updatedReq){
		reqs.get(reqs.indexOf(req)).output=updatedReq.output;
		reqs.get(reqs.indexOf(req)).exitStatus=updatedReq.exitStatus;
		if(def!=null)
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					def.refresh();
				}
			});
	}
	
	public static void removeRequest(Request req){
		reqs.remove(req);
		if(def!=null)
			def.refresh();
	}
	
	class ViewContentProvider implements IStructuredContentProvider {
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
		public void dispose() {
		}
		public Object[] getElements(Object parent) {
			if(cloud==null){
				showMessage("Credentials file not set/invalid. Set it through : Window -> Preferences -> VirtualMachines Manager");
				return new String[] {};
			}
			return reqs.toArray();
		}
	}
	
	public static VMActivity getDefault(){
		return def;
	}

	
	public VMActivity(){
		def=this;
		cloud = vm_manager.Activator.getDefault().getCloud();
	}
	
	@Override
	public void createPartControl(Composite parent) {
		
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		
		createColumns(parent, viewer);
		final Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		viewer.setContentProvider(new ViewContentProvider());
		getSite().setSelectionProvider(viewer);
		viewer.setInput(getViewSite());
	
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 2;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		viewer.getControl().setLayoutData(gridData);
		
		makeActions(parent);
		hookDoubleClickAction();
		hookContextMenu();
	}
	
	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				VMActivity.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}
	
	private void fillContextMenu(IMenuManager manager) {
		manager.add(clear_activity);
		manager.add(save_activity);
		manager.add(see_json_request);
		manager.add(executeAgain);
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}
	
	private void makeActions(Composite parent) {
		
		Actions actions = new Actions(cloud,viewer);
		clear_activity = actions.clear_activity();
		see_json_request = actions.see_json_request();
		save_activity = actions.saveActivityToFile();
		doubleClickActivity = actions.doubleClickActivity();
		executeAgain=actions.executeAgain();
	}
	
	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickActivity.run();
			}
		});
	}
	
	private void createColumns(final Composite parent, final TableViewer viewer) {
		String[] titles = { "Hostname", "Type of request", "Time of Request" , "Status" };
		int[] bounds = { 200, 200, 200 , 300 , 200};
		
		// first column for hostname
		TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0], 0);		
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Request vm = (Request) element;
				return vm.hostname;
			}
			public Image getImage(Object obj) {
				return PlatformUI.getWorkbench().
						getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
			}
		});

		// Second column is for type
		col = createTableViewerColumn(titles[1], bounds[1], 1);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Request vm = (Request) element;
				return vm.type;
			}
		});

		// launch date
		col = createTableViewerColumn(titles[2], bounds[2], 2);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Request vm = (Request) element;
				return vm.launchDateTime;
			}
		});
		
		// save log to file
		col = createTableViewerColumn(titles[3], bounds[3], 3);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Request vm = (Request) element;
				return vm.jobStatus;
			}
		});
	}
	
	private TableViewerColumn createTableViewerColumn(String title, int bound, final int colNumber) {
		final TableViewerColumn viewerColumn = new TableViewerColumn(viewer,SWT.NONE);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		return viewerColumn;
	}

	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(
			viewer.getControl().getShell(),
			"VM Activity",
			message);
	}
	public void refresh(){
		if(cloud==null)
			cloud = vm_manager.Activator.getDefault().getCloud();
		viewer.refresh();
	}
}
