package vm_manager.views;

import java.util.Date;

import net.sf.json.JSONObject;

public class Request {
	public String hostname;
	public String type;
	public String output;
	public int exitStatus;
	public String launchDateTime;
	public String jobStatus;
	public JSONObject request;
	public long timeout;
}
