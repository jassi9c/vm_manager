package vm_manager.views;


import java.util.Map;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.part.*;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.jface.action.*;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.*;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.SWT;

import prometheus.Cloud;
import prometheus.VirtualMachine;

public class VMView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "vm_manager.views.VMView";

	private Cloud cloud;
	private TableViewer viewer;
	
	private static VMView vmView;
	
	private Action start_vm_option;
	private Action stop_vm_option;
	private Action terminate_vm_option;
	private Action reboot_vm_option;
	private Action refresh_list_option;
	private Action execute_shell_command;

	class ViewContentProvider implements IStructuredContentProvider {
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
		public void dispose() {
		}
		public Object[] getElements(Object parent) {
			if(cloud==null){
				showMessage("Credentials file not set/invalid. Set it through : Window -> Preferences -> VirtualMachines Manager");
				return new String[] {};
			}
			return cloud.getAllVirtualMachines().toArray();
		}
	}
	
	public static VMView getDefault(){
		return vmView;
	}
	
	public VMView() {
		try{
			vmView=this;
			cloud = vm_manager.Activator.getDefault().getCloud();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}	
	}
	public void setCloud(Cloud cloud){
		this.cloud=cloud;
	}
	
	private void showMessage(String message) {
		MessageDialog.openInformation(
			viewer.getControl().getShell(),
			"VM View",
			message);
	}
	
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	
	public void createPartControl(Composite parent) {

		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		
		createColumns(parent, viewer);
		final Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		viewer.setContentProvider(new ViewContentProvider());
		getSite().setSelectionProvider(viewer);
		viewer.setInput(getViewSite());
	
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 2;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		viewer.getControl().setLayoutData(gridData);

		makeActions(parent);
		hookContextMenu();

	}

	private void createColumns(final Composite parent, final TableViewer viewer) {
		String[] titles = { "Hostname", "Private IP", "Public IP" , "Status" , "Tags" };
		int[] bounds = { 200, 200, 200 , 100 , 200};
		
		// first column for hostname
		TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0], 0);		
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				VirtualMachine vm = (VirtualMachine) element;
				return vm.getHostname();
			}
			public Image getImage(Object obj) {
				return PlatformUI.getWorkbench().
						getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
			}
		});

		// Second column is for private ip
		col = createTableViewerColumn(titles[1], bounds[1], 1);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				VirtualMachine vm = (VirtualMachine) element;
				return vm.getPrivateIp();
			}
		});

		// Now the public ip
		col = createTableViewerColumn(titles[2], bounds[2], 2);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				VirtualMachine vm = (VirtualMachine) element;
				return vm.getPublicIp();
			}
		});
		
		// status of vm
		col = createTableViewerColumn(titles[3], bounds[3], 3);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				VirtualMachine vm = (VirtualMachine) element;
				return vm.getStatus();
			}
		});
		
		// tags of the vm
		col = createTableViewerColumn(titles[4], bounds[4], 4);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				VirtualMachine vm = (VirtualMachine) element;
				String tags = "";
				try{
					Map<String,String> temp = vm.getTags();
					if(temp!=null){
						for(Map.Entry<String,String> entry : vm.getTags().entrySet() ){
							if(!entry.getKey().equals("hostname") && !entry.getKey().equals("Name"))
								tags+=entry.getKey()+":"+entry.getValue()+"\n";
						}
						if(tags.length()>0){
							return tags.substring(0, tags.length()-1);
						}
					}
				}catch(Exception e){
					e.printStackTrace();
					System.out.println(vm.getHostname()+"Exception in getText() in VMView for hostname : "+vm.getHostname()+"\n"+e.getMessage());
				}

				return "";
			}
		});

	}
	
	private TableViewerColumn createTableViewerColumn(String title, int bound, final int colNumber) {
		final TableViewerColumn viewerColumn = new TableViewerColumn(viewer,SWT.NONE);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		return viewerColumn;
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				VMView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}
	
	private void fillContextMenu(IMenuManager manager) {	
		manager.add(start_vm_option);
		manager.add(stop_vm_option);
		manager.add(reboot_vm_option);
		manager.add(terminate_vm_option);
		manager.add(refresh_list_option);
		manager.add(execute_shell_command);
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}
	
	private void makeActions(Composite parent) {
		
		Actions actions = new Actions(cloud,viewer);
		start_vm_option = actions.start_vm_option("Start", "Start this vm"); 
		stop_vm_option = actions.stop_vm_option("Stop", "Stop this vm");
		terminate_vm_option = actions.terminate_vm_option("Terminate", "Terminate the vm");
		reboot_vm_option= actions.reboot_vm_option("Reboot", "Reboot the vm");
		execute_shell_command = actions.execute_shell_command("Execute shell command", "Execute a shell command on the selected vm(s)");
		refresh_list_option = new Action() {
			public void run(){
				refresh();
			}
		};
		refresh_list_option.setText("Refresh");
		
	}
	public void refresh(){
		if(cloud==null)
			cloud = vm_manager.Activator.getDefault().getCloud();
		viewer.refresh();
	}
}