package vm_manager.views;


import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import prometheus.AddEntryToDNSServers;
import prometheus.Cloud;
import prometheus.RemoteExecutionResponse;
import prometheus.RunScriptOnInstance;
import prometheus.VirtualMachine;
import prometheus.WaitForVmToBootUp;
import vm_manager.Activator;
import vm_manager.wizards.ExecuteCommandWizard;
import vm_manager.wizards.ExecuteScriptWizard;

public class Actions {
	
	private Cloud cloud;
	private TableViewer viewer;
	private JFrame frame;
	
	public Actions(Cloud cloud ,TableViewer viewer){
		this.cloud=cloud;
		this.viewer=viewer;
	}
	
	protected Action clear_activity(){
		Action temp = new Action() {
			public void run() {
				if(cloud==null)
					cloud=vm_manager.Activator.getDefault().getCloud();
				ISelection selection = viewer.getSelection();
				Iterator<IStructuredSelection> iterator = ((IStructuredSelection)selection).iterator();
				while(iterator.hasNext()){
					Object obj = iterator.next();
					vm_manager.views.VMActivity.removeRequest((Request)obj);
				}
			}
		};
		temp.setText("Clear");
		return temp;
	}
	
	protected Action doubleClickActivity(){
		Action temp = new Action() {
			public void run() {
				if(cloud==null)
					cloud=vm_manager.Activator.getDefault().getCloud();
				ISelection selection = viewer.getSelection();
				Iterator<IStructuredSelection> iterator = ((IStructuredSelection)selection).iterator();
				while(iterator.hasNext()){
					Object obj = iterator.next();
					Request req = (Request)obj;
					String output = req.jobStatus.equals("Running")?"Job still going on":req.output;
					//MessageDialog.o
					MessageDialog.open(MessageDialog.INFORMATION,vm_manager.views.VMActivity.getDefault().getSite().getShell(), req.hostname, "The json request object is : \n\n"+printJSON(req.request)+"\n\nThe output is :\n"+output,SWT.V_SCROLL);
				}
			}
		};
		return temp;
	}
	
	private String printJSON(JSONObject obj){
		String niceformatString = "";
		JSONArray ite = obj.names();
		int i=0;
		while(i<ite.size()){
			niceformatString+=ite.get(i)+"\t\t";
			if(obj.get(ite.get(i)) instanceof String)
				niceformatString+=obj.getString((String)ite.get(i));
			else //if(obj.get(ite.next()) instanceof JSONObject)
				niceformatString+=obj.get(ite.get(i)).toString();
			niceformatString+="\n";
			i++;
		}
		return niceformatString;
	}
	
	protected Action executeAgain(){
		Action temp = new Action() {
			public void run(){
				if(cloud==null)
					cloud=vm_manager.Activator.getDefault().getCloud();
				ISelection selection = viewer.getSelection();
				Iterator<IStructuredSelection> iterator = ((IStructuredSelection)selection).iterator();
				while(iterator.hasNext()){
					Object obj = iterator.next();
					Request req = (Request)obj;
					Shell shell = vm_manager.views.VMActivity.getDefault().getSite().getShell();
					if(!(req.type.equals("Execute command") || req.type.equals("Execute script"))){
						return;
					}
					if(req.type.equals("Execute command")){
						ExecuteCommandWizard wiz = new ExecuteCommandWizard(cloud, req.hostname);
						wiz.executeCommand(req.hostname, req.request.getString("command"), req.timeout);
					}
					else if(req.type.equals("Execute script")){
						ExecuteScriptWizard wiz = new ExecuteScriptWizard(cloud, req.request.getString("sourceFile"));
						wiz.executeScript(req.hostname);
					}
				}
			}
		};
		temp.setText("Execute this again");
		return temp;
	}
	
	protected Action saveActivityToFile(){
		Action temp = new Action() {
			public void run() {
				if(cloud==null)
					cloud=vm_manager.Activator.getDefault().getCloud();
				ISelection selection = viewer.getSelection();
				Iterator<IStructuredSelection> iterator = ((IStructuredSelection)selection).iterator();
				while(iterator.hasNext()){
					Object obj = iterator.next();
					Request req = (Request)obj;
					Shell shell = vm_manager.views.VMActivity.getDefault().getSite().getShell();
					SafeSaveFileDialog saveDialog = new SafeSaveFileDialog(shell);
					String path = saveDialog.open();
					if(path==null)
						return;
					System.out.println(path);
					try {
						String contents =  "";
						contents+="Hostname : "+req.hostname;
						contents+="\n";
						contents+="Request Type :"+req.type;
						contents+="\n";
						contents+="Launch Time : "+req.launchDateTime;
						contents+="\n";
						contents+="JSON Request object :\n"+req.request.toString();
						contents+="\n";
						contents+="Exit Status : "+req.exitStatus;
						contents+="\n";
						contents+="Output : \n"+req.output;
						contents+="\n\n";
						FileUtils.writeStringToFile(new File(path), contents);
					} catch (IOException e) {
						MessageDialog.openError(shell, "", "Error while saving to file : "+e.getMessage());
					}

				}
			}
		};
		temp.setText("Save activity to file");
		return temp;
	}
	
	protected Action see_json_request(){
		Action temp = new Action() {
			public void run() {
				if(cloud==null)
					cloud=vm_manager.Activator.getDefault().getCloud();
				ISelection selection = viewer.getSelection();
				Iterator<IStructuredSelection> iterator = ((IStructuredSelection)selection).iterator();
				while(iterator.hasNext()){
					Object obj = iterator.next();
					Request req = (Request)obj;
					MessageDialog.openInformation(vm_manager.views.VMActivity.getDefault().getSite().getShell(), req.hostname, "The json request object is : \n\n"+printJSON(req.request));

				}
			}
		};
		temp.setText("View json object of this request");
		return temp;
	}
	
		protected Action stop_vm_option(String label , String toolTip){
		Action temp = new Action() {
			public void run() {
				
				if(cloud==null)
					cloud=vm_manager.Activator.getDefault().getCloud();
				
				ISelection selection = viewer.getSelection();
				Iterator<IStructuredSelection> iterator = ((IStructuredSelection)selection).iterator();
				while(iterator.hasNext()){
				Object obj = iterator.next();
				VirtualMachine vm = (VirtualMachine)obj;
				if(vm.getStatus().equals("stopped"))
					return;
				final String hostname = vm.getHostname();

				cloud.getVirtualMachine(hostname).setStatus("stopping...");
				vm_manager.views.VMView.getDefault().refresh();				
				Job job = new Job("Stopping VM : "+hostname) {
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						try {
							
							Request vm_ac_req = new Request();
							vm_ac_req.hostname=hostname;
							vm_ac_req.type="Shut down VM";
							vm_ac_req.jobStatus="Running";
							JSONObject details = new JSONObject();
							details.put("Action", "shutdown");
							vm_ac_req.request=details;
							vm_manager.views.VMActivity.addRequest(vm_ac_req);
							Request ori_req = vm_ac_req;
							
							cloud.stopVM(hostname);
							
							vm_ac_req.jobStatus="Finished";
							vm_ac_req.exitStatus=0;
							vm_ac_req.output="";
							vm_manager.views.VMActivity.updateRequest(ori_req, vm_ac_req);
							
							vm_manager.views.VMActivity.getDefault().refresh();
							
						} catch (Exception e) {
							showMessage("Could not stop vm. Exception : "+e.getMessage());
							e.printStackTrace();
						}
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								viewer.refresh();
							}
						});
						return Status.OK_STATUS;
					}
				};
				job.schedule();
				}
			}
		};
		temp.setText(label);
		temp.setToolTipText(toolTip);
		return temp;
	}

	protected Action start_vm_option(String label , String toolTip){
		Action temp = new Action() {
			public void run() {
				if(cloud==null)
					cloud=vm_manager.Activator.getDefault().getCloud();
				ISelection selection = viewer.getSelection();
				Iterator<IStructuredSelection> iterator = ((IStructuredSelection)selection).iterator();
				while(iterator.hasNext()){
				Object obj = iterator.next();//((IStructuredSelection)selection).getFirstElement();
				VirtualMachine vm = (VirtualMachine)obj;
				if(vm.getStatus().equals("running"))
					return;
				final String hostname = vm.getHostname();

				cloud.getVirtualMachine(hostname).setStatus("starting...");
				vm_manager.views.VMView.getDefault().refresh();
				
				Job job = new Job("Starting VM : "+hostname) {
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						try {
							
							Request vm_ac_req = new Request();
							vm_ac_req.hostname=hostname;
							vm_ac_req.type="Start VM";
							vm_ac_req.jobStatus="Running";
							JSONObject details = new JSONObject();
							details.put("Action", "startVM");
							vm_ac_req.request=details;
							vm_manager.views.VMActivity.addRequest(vm_ac_req);
							Request ori_req = vm_ac_req;	
							cloud.startVM(hostname);
							String[] dnsServers = cloud.getVirtualMachine(hostname).getListOfDNSServers();
							if(dnsServers!=null && cloud.getVirtualMachine(hostname).isThisADNSServer()==false){
								String server = dnsServers[0];
								
								JSONObject request = new JSONObject();
								JSONArray addEntryToDNSServers = new JSONArray();
								JSONObject addEntryToDNSServer  = new JSONObject();
								addEntryToDNSServer.put("dnsServer", server);
								addEntryToDNSServer.put("dnsFile", "/var/named/emory.edu");
								addEntryToDNSServers.add(addEntryToDNSServer);
								request.put("addEntryToDNSServers", addEntryToDNSServers);
								request.put("hostname", hostname);
								request.put("username", "ec2-user");
								String sshKeyPath = vm_manager.Activator.getDefault().getPreferenceStore().getString("sshFolderPreference")+"/"+cloud.getVirtualMachine(server).getKeyName()+".pem";
								request.put("sshKeyPath", sshKeyPath);
								
								AddEntryToDNSServers addEntryReq = new AddEntryToDNSServers(cloud,request);
								RemoteExecutionResponse  addEntryReqResponse = addEntryReq.execute();
							}
							vm_ac_req.jobStatus="Finished";
							vm_ac_req.exitStatus=0;
							vm_ac_req.output="";
							vm_manager.views.VMActivity.updateRequest(ori_req, vm_ac_req);
							vm_manager.views.VMActivity.getDefault().refresh();
						} catch (Exception e) {
							showMessage("Could not start vm. Exception : "+e.getMessage());
							e.printStackTrace();
						}
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								vm_manager.views.VMView.getDefault().refresh();
							}
						});
						return Status.OK_STATUS;
					}
				};
				job.schedule();
				}
			}
		};

		temp.setText(label);
		temp.setToolTipText(toolTip);
		return temp;
	}
	
	protected Action terminate_vm_option(String label , String toolTip){
		Action temp = new Action() {
			public void run() {
				if(cloud==null)
					cloud=vm_manager.Activator.getDefault().getCloud();
				ISelection selection = viewer.getSelection();
				Iterator<IStructuredSelection> iterator = ((IStructuredSelection)selection).iterator();
				while(iterator.hasNext()){
				Object obj = iterator.next();//((IStructuredSelection)selection).getFirstElement();
				VirtualMachine vm = (VirtualMachine)obj;
				final String hostname = vm.getHostname();

				cloud.getVirtualMachine(hostname).setStatus("terminating...");
				vm_manager.views.VMView.getDefault().refresh();
				
				Job job = new Job("Terminating VM : "+hostname) {
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						try {
						Request vm_ac_req = new Request();
						vm_ac_req.hostname=hostname;
						vm_ac_req.type="Terminate VM";
						vm_ac_req.jobStatus="Running";
						JSONObject details = new JSONObject();
						details.put("Action", "terminate");
						vm_ac_req.request=details;
						vm_manager.views.VMActivity.addRequest(vm_ac_req);
						Request ori_req = vm_ac_req;
							cloud.terminateVM(hostname);
							vm_ac_req.jobStatus="Finished";
							vm_ac_req.exitStatus=0;
							vm_ac_req.output="";
							vm_manager.views.VMActivity.updateRequest(ori_req, vm_ac_req);
						} catch (Exception e) {
							showMessage("Could not terminate vm. Exception : "+e.getMessage());
							e.printStackTrace();
						}
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								viewer.refresh();
							}
						});
						return Status.OK_STATUS;
					}
				};
				job.schedule();
				}
			}
		};
		temp.setText(label);
		temp.setToolTipText(toolTip);
		return temp;

	}
	
	protected Action reboot_vm_option(String label , String toolTip){
		Action temp = new Action() {
			public void run() {
				if(cloud==null)
					cloud=vm_manager.Activator.getDefault().getCloud();
				ISelection selection = viewer.getSelection();
				Iterator<IStructuredSelection> iterator = ((IStructuredSelection)selection).iterator();
				while(iterator.hasNext()){
				Object obj = iterator.next();//((IStructuredSelection)selection).getFirstElement();
				VirtualMachine vm = (VirtualMachine)obj;
				final String hostname = vm.getHostname();

				cloud.getVirtualMachine(hostname).setStatus("rebooting...");
				viewer.refresh();
				
				Job job = new Job("Rebooting VM : "+hostname) {
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						try {
							Request vm_ac_req = new Request();
							vm_ac_req.hostname=hostname;
							vm_ac_req.type="Reboot VM";
							vm_ac_req.jobStatus="Running";
							JSONObject detailsT = new JSONObject();
							detailsT.put("Action", "reboot");
							vm_ac_req.request=detailsT;
							vm_manager.views.VMActivity.addRequest(vm_ac_req);
							Request ori_req = vm_ac_req;
							
							cloud.rebootVM(hostname);
							VirtualMachine vm = cloud.getVirtualMachine(hostname);
							JSONObject details = new JSONObject();
							details.put("hostname", hostname);
							details.put("username", "ec2-user");
							details.put("sshKeyPath", "/home/jaspal/gsoc_demo/gsoc12_jaspal.pem");
							WaitForVmToBootUp wait = new WaitForVmToBootUp(cloud, details);
							wait.execute();
							cloud.getVirtualMachine(hostname).setStatus("running");
							vm_ac_req.jobStatus="Finished";
							vm_ac_req.exitStatus=0;
							vm_ac_req.output="";
							vm_manager.views.VMActivity.updateRequest(ori_req, vm_ac_req);
						} catch (Exception e) {
							showMessage("Could not reboot vm. Exception : "+e.getMessage());
							e.printStackTrace();
						}
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								viewer.refresh();
							}
						});
						return Status.OK_STATUS;
					}
				};
				job.schedule();
				}
			}
		};

		temp.setText(label);
		temp.setToolTipText(toolTip);
		return temp;
	}
	
	protected Action execute_shell_command(String label , String toolTip){
		Action temp = new Action() {
			public void run(){
				if(cloud==null)
					cloud=vm_manager.Activator.getDefault().getCloud();
				ISelection selection = viewer.getSelection();
				ArrayList<String> vms = new ArrayList<String>();
				Iterator<IStructuredSelection> iterator = ((IStructuredSelection)selection).iterator();
				while(iterator.hasNext()){
					Object obj = iterator.next();
					VirtualMachine vm = (VirtualMachine)obj;
					if(vm.getStatus().equals("running"))
						vms.add(vm.getHostname());
				}
				if(vms.size()==0)
					return;
				Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
				WizardDialog wizardDialog = new WizardDialog(shell, new ExecuteCommandWizard(cloud,vms));

				if (wizardDialog.open() == Window.OK) {
				} else {
				}

			}
		};
		temp.setText(label);
		temp.setToolTipText(toolTip);
		return temp;
	}
	
	private void showMessage(String message) {
		System.out.println(message);
		/*
		MessageDialog.openInformation(
			viewer.getControl().getShell(),
			"VirtualMachine Manager",
			message);
			*/
	}
}


