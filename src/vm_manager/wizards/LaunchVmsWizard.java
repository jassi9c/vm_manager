package vm_manager.wizards;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.codehaus.jackson.impl.ReaderBasedParser;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import prometheus.Cloud;
import prometheus.LaunchInstances;
import vm_manager.views.Request;
import vm_manager.views.VMView;

public class LaunchVmsWizard extends Wizard implements INewWizard{
	
	private Cloud cloud;
	private AddVmsWizardPage vmPage;
	
	public LaunchVmsWizard(Cloud cloud){
		super();
		this.cloud=cloud;
		setNeedsProgressMonitor(true);
	}
	
	public void addPages() {
		vmPage = new AddVmsWizardPage(cloud);
		addPage(vmPage);
	}
	
	@Override
	public boolean performFinish() {
		
		for(int i=0;i<vmPage.num_reqs;i++){
			
			final JSONObject details = new JSONObject();
			details.put("hostname", vmPage.hostnames[i].getText());
			details.put("sshKey", vmPage.keys[i].getText());
			String sshKeyPath = vm_manager.Activator.getDefault().getPreferenceStore().getString("sshFolderPreference")+"/"+vmPage.keys[i].getText()+".pem";
			details.put("sshKeyPath", sshKeyPath);
			details.put("username", "ec2-user");
			details.put("ami", vmPage.amis[i].getText());
			details.put("securityGroup", vmPage.groups[i].getText());
			details.put("type", "t1.micro");
			
			JSONArray dnsServers = new JSONArray();
			dnsServers.add(vmPage.dnsServers[i].getText());
			details.put("overrideDNS", dnsServers);
			
			JSONArray addEntryToDNSServers = new JSONArray();
			JSONObject addEntryToDNSServer  = new JSONObject();
			addEntryToDNSServer.put("dnsServer", vmPage.dnsServers[i].getText());
			addEntryToDNSServer.put("dnsFile", "/var/named/emory.edu");
			addEntryToDNSServers.add(addEntryToDNSServer);
			
			if(!vmPage.scripts[i].getText().equals("None")){
				details.put("startup_script", vmPage.scripts[i].getText());
			}
			
			details.put("addEntryToDNSServers", addEntryToDNSServers);
			
			
			try{
				final String hostname = vmPage.hostnames[i].getText();
				Job job = new Job("Launching VM : "+hostname){

					LaunchInstances req = null;
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						try {
							
							Request vm_ac_req = new Request();
							vm_ac_req.hostname=hostname;
							vm_ac_req.type="Launch VM";
							vm_ac_req.jobStatus="Running";
							vm_ac_req.request=details;
							vm_manager.views.VMActivity.addRequest(vm_ac_req);
							Request ori_req = vm_ac_req;
							
							req = new LaunchInstances(details, cloud);
							Thread temp = new Thread(req);
							monitor.beginTask("Executing the launch vm request", req.getTotalNumberOfStatus());
							String lastStatus = req.getStatus();
							monitor.subTask(lastStatus);
							temp.start();
							while(temp.isAlive()){
								for(int i=0;i<req.getTotalNumberOfStatus()-3;i++){
									while(req.getStatus().equals(lastStatus)){
										Thread.sleep(100);
										if(!temp.isAlive())
											break;
										if(monitor.isCanceled()){
											monitor.setCanceled(true);
											temp.stop();
											this.done(Status.CANCEL_STATUS);
										}
									}
									if(!temp.isAlive())
										break;
									lastStatus=req.getStatus();
									monitor.subTask(lastStatus);
									monitor.worked(1);
									if(req.getStatus().equals("Finished."))
										break;
									if(monitor.isCanceled()){
										monitor.setCanceled(true);
										temp.stop();
										this.done(Status.CANCEL_STATUS);
									}

								}
							}
							monitor.done();
							Display.getDefault().syncExec(new Runnable() {
								public void run() {
									VMView view = vm_manager.views.VMView.getDefault();
									if(view!=null)
										view.refresh();
								}
							});
							vm_ac_req.jobStatus="Finished";
							vm_ac_req.exitStatus=req.getResult().getReturnStatus();
							vm_ac_req.output=req.getResult().getOutput();
							vm_manager.views.VMActivity.updateRequest(ori_req, vm_ac_req);
							this.done(Status.OK_STATUS);
						} catch (Exception e) {
							e.printStackTrace();
							Display.getDefault().syncExec(new Runnable() {
								public void run() {
									VMView view = vm_manager.views.VMView.getDefault();
									if(view!=null)
										view.refresh();
								}
							});
							this.done(Status.CANCEL_STATUS);
						}
						return Status.OK_STATUS;

					}
					
				};
				job.schedule();
			}catch(Exception e){
				
			}
		}
		
		return true;
	}
	
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		
	}

}
