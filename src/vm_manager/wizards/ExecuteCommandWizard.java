package vm_manager.wizards;

import java.util.ArrayList;

import net.sf.json.JSONObject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import prometheus.Cloud;
import prometheus.RemoteExecutionResponse;
import prometheus.VirtualMachine;
import vm_manager.views.Request;

public class ExecuteCommandWizard extends Wizard implements INewWizard{

	private Cloud cloud;
	private ArrayList<String> vms;
	private InputScriptCommandWizardPage commandPage;
	
	
	public ExecuteCommandWizard(Cloud cloud,ArrayList<String> vms){
		super();
		this.cloud=cloud;
		setNeedsProgressMonitor(true);	
		this.vms=vms;
	}
	
	public ExecuteCommandWizard(Cloud cloud , String hostname){
		super();
		this.cloud=cloud;
		setNeedsProgressMonitor(true);
		vms = new ArrayList<String>();
		vms.add(hostname);
	}

	public void addPages() {
		commandPage = new InputScriptCommandWizardPage(cloud, false, true);
		addPage(commandPage);
	}
	
	@Override
	public boolean performFinish() {
		
		boolean vmsSelected = false;
		long timeout = 0;
		for(String vm : vms){
				try{
					timeout = Long.parseLong(commandPage.timeout.getText());
				}catch(final Exception e){
					Display.getDefault().syncExec(new Runnable() {
						public void run() {
							MessageDialog.openError(getShell(), "Response", "Please enter a number for timeout.\nThe exact error is "+e.getMessage());
						}
					});
					return false;
				}
				executeCommand(vm,commandPage.commandBox.getText(),timeout*1000);
		}
		return true;
	}

	public void executeCommand(final String hostname, String command,final long timeout){
		VirtualMachine vm = cloud.getVirtualMachine(hostname);
		String sshKey = vm_manager.Activator.getDefault().getPreferenceStore().getString("sshFolderPreference")+"/"+vm.getKeyName()+".pem";
		
		final JSONObject details = new JSONObject();
		
		details.put("host", vm.getPublicIp());
		details.put("sshKeyPath", sshKey);
		details.put("username", "ec2-user");
		details.put("command", command);
		
		try{
			Job job = new Job("Executing command on "+hostname){
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					try{
						System.out.println("executeCommand called with :\n"+details.toString());
						
						Request vm_ac_req = new Request();
						vm_ac_req.hostname=hostname;
						vm_ac_req.type="Execute command";
						vm_ac_req.jobStatus="Running";
						vm_ac_req.request=details;
						vm_ac_req.timeout=timeout;
						vm_manager.views.VMActivity.addRequest(vm_ac_req);
						Request ori_req = vm_ac_req;
						
						RemoteExecutionResponse response = null;
						try {
							response =   cloud.runRemoteCommand(details,timeout);							
						} catch (Exception e) {
							System.out.println(e.getMessage());
							e.printStackTrace();
						}
						/*
						class MyRunnable implements Runnable{
							RemoteExecutionResponse response;
							public RemoteExecutionResponse returnRes(){
								return response;
							}
							public void run(){
								try {
									System.out.println("calling runRemoteCommand");
									response =   cloud.runRemoteCommand(details);							
								} catch (Exception e) {
									System.out.println(e.getMessage());
									e.printStackTrace();
								}
							}
						};
						
						MyRunnable runnable =  new MyRunnable();
						Thread thread = new Thread(runnable);
						long startTime = System.currentTimeMillis();
						thread.start();
						while(thread.isAlive() && (System.currentTimeMillis() - startTime < timeout)){
							Thread.sleep(1000);
							//System.out.println(System.currentTimeMillis() - startTime + " timeout : "+timeout);
						}
						thread.stop();
						response = runnable.returnRes();
						*/
						//final RemoteExecutionResponse response =   cloud.runRemoteCommand(details);
						/*
						Display.getDefault().syncExec(new Runnable() {
							public void run() {
								MessageDialog.openInformation(getShell(), "Response", response.getOutput());
							}
						});
						*/
						vm_ac_req.jobStatus="Finished";
						vm_ac_req.exitStatus=response.getReturnStatus();
						vm_ac_req.output=response.getOutput();
						vm_manager.views.VMActivity.updateRequest(ori_req, vm_ac_req);
						
					}catch(Exception e){
						System.out.println("Could not execute the command");
						System.out.println(details.toString());
						e.printStackTrace();
						return Status.CANCEL_STATUS;
					}
					return Status.OK_STATUS;
				}
			};
			job.schedule();
			
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		
	}

}
