package vm_manager.wizards;

import java.util.ArrayList;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import prometheus.AmazonCloud;
import prometheus.Cloud;
import prometheus.VirtualMachine;

public class InputBasicsWizardPage extends WizardPage{

	protected Composite container;
	protected Text hostname;
	protected Label amiIDLabel;
	protected Text amiIDText;
	protected Combo sshKeyCombo;
	protected Text username;
	protected Combo securityGroupCombo;
	protected Combo typesCombo;
	protected Combo dnsServersCombo;
	protected String startupScript;
	protected Button scriptButton;
	protected boolean isNormalRequest;
	
	protected Cloud cloud;

	public InputBasicsWizardPage(String desc,Cloud cloud,boolean isNormalRequest) {
		super("Set basic parameters");
		setTitle("Set basic parameters");
		setDescription(desc);
		this.cloud=cloud;
		this.isNormalRequest=isNormalRequest;
	}
	
	@Override
	public void createControl(Composite parent) {
	
		container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 2;
		
		
		// Hostname field
		
		Label label1 = new Label(container, SWT.NULL);
		label1.setText("The hostname of the vm");
		hostname = new Text(container, SWT.BORDER | SWT.SINGLE);
		hostname.setText("");
		hostname.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
			}
			@Override
			public void keyReleased(KeyEvent e) {
				if (!hostname.getText().isEmpty()) {
					setPageComplete(true);
				}
			}
		});		
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		hostname.setLayoutData(gd);
		//
		
		// AMI - ID
		Label amiLabel = new Label(container,SWT.NULL);
		amiLabel.setText("AMI of the vm");
		if(!isNormalRequest)	// layout for DNS server
		{
		amiIDLabel = new Label(container,SWT.BORDER | SWT.SINGLE);
		amiIDLabel.setText("ami-4b943b22");
		amiIDLabel.setLayoutData(gd);
		}else
		{
			amiIDText = new Text(container,SWT.BORDER | SWT.SINGLE);
			amiIDText.setText("");
			amiIDText.setLayoutData(gd);
		}
		//
		
		// username
		Label usernameLabel = new Label(container,SWT.NULL);
		usernameLabel.setText("Username to be used in remote ssh");
		username = new Text(container,SWT.BORDER | SWT.SINGLE);
		username.setText("ec2-user");
		username.setLayoutData(gd);
		//
		
		// sshKey
		Label sshKeyLabel = new Label(container,SWT.NULL);
		sshKeyLabel.setText("Name of sshKey ");
		sshKeyCombo = new Combo(container,SWT.READ_ONLY);
		sshKeyCombo.setItems(((AmazonCloud)cloud).getKeyPairNames());
		sshKeyCombo.select(0);
		//
		
		// securityGroup
		Label securityGroupLabel = new Label(container,SWT.NULL);
		securityGroupLabel.setText("Name of security group ");
		
		securityGroupCombo = new Combo(container,SWT.READ_ONLY);
		securityGroupCombo.setItems(((AmazonCloud)cloud).getSecurityGroups());
		securityGroupCombo.select(0);
		//
		
		// type 
		Label typeLabel = new Label(container,SWT.NULL);
		typeLabel.setText("Size of instance");
		String[] types = {
				"Micro (t1.micro)" ,
				"Small (m1.small)" , 
				"High CPU Medium (c1.medium)" ,
				"Medium (m1.medium)" , 
				"Large (m1.large)" ,
				"Extra Large (m1.xlarge)" ,
				"High Memory Extra Large (m2.xlarge)"
		};
		typesCombo = new Combo(container,SWT.READ_ONLY);
		typesCombo.setItems(types);
		typesCombo.select(0);
		//
		
		// dns server
		if(isNormalRequest){
			Label dnsLabel = new Label(container,SWT.NULL);
			dnsLabel.setText("Choose dns server");
			
			ArrayList<VirtualMachine> dnsServers = cloud.getAllOnlineDNSServers();
			
			String dnsServersHostnames[] = new String[dnsServers.size()+1];
			int i = 0;
			for(i=0;i<dnsServers.size();i++)
				dnsServersHostnames[i]=dnsServers.get(i).getHostname();
			
			dnsServersHostnames[i]="Launch new DNS Server";
			dnsServersCombo= new Combo(container, SWT.READ_ONLY);
			dnsServersCombo.setItems(dnsServersHostnames);
			dnsServersCombo.select(0);
		}
		
		// startup script
		if(isNormalRequest){
			Label scriptLabel = new Label(container,SWT.NONE);
			scriptLabel.setText("Startup script");
			scriptButton = new Button(container,SWT.PUSH);
			scriptButton.setText("Click to choose");
			scriptButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			scriptButton.addSelectionListener(new SelectionAdapter(){
				public void widgetSelected(SelectionEvent e) {
					Button button = (Button)e.getSource();
					if(startupScript==null){
					FileDialog dialog = new FileDialog(getShell(), SWT.NULL);
					String path = dialog.open();
					startupScript = path;
					button.setText("Startup script : "+path.substring(path.lastIndexOf("/")+1)+". Click to clear.");
					}else{
						startupScript = null;
						button.setText("Click to choose");
					}
				}
				
			});
		}
		
		startupScript = null;
		
		// Required to avoid an error in the system
		setControl(container);
		setPageComplete(false);
	}
	
}
