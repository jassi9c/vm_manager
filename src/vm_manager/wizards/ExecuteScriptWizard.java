package vm_manager.wizards;

import net.sf.json.JSONObject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import prometheus.*;
//import prometheus.VirtualMachine;
import vm_manager.views.Request;

public class ExecuteScriptWizard extends Wizard implements INewWizard{

	private Cloud cloud;
	private SelectVmsPage selectVmsPage;
	private String scriptPath;
	
	public ExecuteScriptWizard(Cloud cloud , String scriptPath){
		super();
		this.cloud=cloud;
		setNeedsProgressMonitor(true);
		this.scriptPath=scriptPath;
	}
	
	public void addPages() {
		selectVmsPage = new SelectVmsPage("For execution of this script", cloud);
		addPage(selectVmsPage);
	}
	
	@Override
	public boolean performFinish() {
		System.out.println("Ready to be executed");
		boolean vmsSelected = false;
		for(Button button: selectVmsPage.selectionButtons){
			if(button!=null && button.getSelection()){
				vmsSelected = true;
				System.out.println(button.getText());
				executeScript(button.getText());
			}
		}

		selectVmsPage.setPageComplete(vmsSelected);
		return vmsSelected;

	}
	
	
	public void executeScript(final String hostname){
		VirtualMachine vm = cloud.getVirtualMachine(hostname);
		String sshKey = vm_manager.Activator.getDefault().getPreferenceStore().getString("sshFolderPreference")+"/"+vm.getKeyName()+".pem";
		
		final JSONObject details = new JSONObject();
		
		details.put("hostname", hostname);
		details.put("sshKeyPath", sshKey);
		details.put("sourceFile", scriptPath);
		details.put("targetFile", "~"+scriptPath.substring(scriptPath.lastIndexOf("/")));
		details.put("username", "ec2-user");
		try {
			final RunScriptOnInstance runScript = new RunScriptOnInstance(cloud, details);
			//final String hostname = hostname;
			Job job = new Job("Executing script on "+hostname){
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					try {
						System.out.println(details.toString());
						
						Request vm_ac_req = new Request();
						vm_ac_req.hostname=hostname;
						vm_ac_req.type="Execute script";
						vm_ac_req.jobStatus="Running";
						vm_ac_req.request=details;
						vm_manager.views.VMActivity.addRequest(vm_ac_req);
						Request ori_req = vm_ac_req;
						
						final RemoteExecutionResponse response =   runScript.execute();
						
						vm_ac_req.jobStatus="Finished";
						vm_ac_req.exitStatus=response.getReturnStatus();
						vm_ac_req.output=response.getOutput();
						vm_manager.views.VMActivity.updateRequest(ori_req, vm_ac_req);
						/*
						Display.getDefault().syncExec(new Runnable() {
							public void run() {
								MessageDialog.openInformation(getShell(), "Response", response.getOutput());
							}
						});
						*/
					} catch (Exception e) {
						System.out.println("Could not execute the script");
						System.out.println(details.toString());
						e.printStackTrace();
						return Status.CANCEL_STATUS;
					}
					return Status.OK_STATUS;
				}
			};
			job.schedule();
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		}

}
