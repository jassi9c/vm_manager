package vm_manager.wizards;

import java.util.Collection;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.*;

import prometheus.Cloud;
import prometheus.VirtualMachine;

public class SelectVmsPage extends WizardPage{

	protected Cloud cloud;
	protected Composite container;
	
	protected Button[] selectionButtons;
	
	public SelectVmsPage(String desc , Cloud cloud){
		super("Select vm(s)");
		setTitle("Select vm(s)");
		setDescription(desc);
		this.cloud=cloud;
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 1;
		
		Collection<VirtualMachine> vmHostnames = cloud.getAllVirtualMachines();
		Label selectionlabel = new Label(container,SWT.NONE);
		selectionlabel.setText("Tick to select");
		//Label vmHostnameLabel = new Label(container,SWT.NONE);
		//vmHostnameLabel.setText("Hostname of vm");
		
		selectionButtons = new Button[vmHostnames.size()];
		
		Object[] vms = vmHostnames.toArray();
		
		for(int i=0,j=0;i<vmHostnames.size();i++){
			VirtualMachine vm = (VirtualMachine)vms[i];
			if(vm.getStatus().equals("running")){
				selectionButtons[j]= new Button(container, SWT.CHECK);
				selectionButtons[j].setText(vm.getHostname());
				j++;
			}
		}
			
		setControl(container);
		setPageComplete(true);
	}

}
