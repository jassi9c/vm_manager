package vm_manager.wizards;

import java.util.Collection;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.hamcrest.core.IsInstanceOf;

import prometheus.Cloud;
import prometheus.VirtualMachine;

public class SelectVmsStartStopTerminateWizardPage extends WizardPage{

	protected Cloud cloud;
	protected Composite container;
	
	protected Button[] selectionButtons;
	protected Label[] presentStates;
	
	protected Composite[] newStates;
	protected Button[] innerButton1;
	protected Button[] innerButton2;
	
	protected Button[] scriptChooserButton;
	protected String[] jobScripts; 
	
	public SelectVmsStartStopTerminateWizardPage(Cloud cloud) {
		super("Start/Stop/Terminate VMs");
		setTitle("Options");
		setDescription("Select vm(s) and operations to perform.");
		this.cloud=cloud;
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 4;
		container.setLayout(layout);
		
		Collection<VirtualMachine> vmHostnames = cloud.getAllVirtualMachines();
		Object[] vms = vmHostnames.toArray();

		int size = vmHostnames.size();
		selectionButtons = new Button[size];
		presentStates = new Label[size];
		newStates = new Composite[size];
		innerButton1 = new Button[size];
		innerButton2 = new Button[size];
		jobScripts = new String[size];
		scriptChooserButton = new Button[size];
		
		String labels[] = new String[] {
				"Tick to select" , 
				"Present state" , 
				"     New State" ,
				"Script as pre-job"
		}; 
		
		for(String option : labels){
			Label label = new Label(container,SWT.NONE);
			label.setText(option);
			
		}
		//Label labels[] = new Label[4];
		for(int i=0;i<size;i++){
			
			final int tempI = i;
			VirtualMachine vm = (VirtualMachine)vms[i];
			
			//if(vm.getStatus().equals("running") || vm.getStatus().equals("running"))
			
			selectionButtons[i]= new Button(container, SWT.CHECK);
			selectionButtons[i].setText(vm.getHostname());
			
			selectionButtons[i].addSelectionListener(new SelectionAdapter(){
				private int i = tempI;
				public void widgetSelected(SelectionEvent e) {
					  Button button = (Button)e.getSource();
					  boolean state = button.getSelection();
					  presentStates[i].setEnabled(state);
					  newStates[i].setVisible(state);
					  innerButton1[i].setGrayed(state);
					  innerButton2[i].setGrayed(state);
					  scriptChooserButton[i].setEnabled(state);
					  if(state==false){
						  scriptChooserButton[i].setText("Click to choose");
						  jobScripts[i]=null;
					  }else{
						  innerButton1[i].setSelection(true);
					  }
				}
			});
			
			
			presentStates[i] = new Label(container,SWT.NONE);
			presentStates[i].setText(vm.getStatus());
			
			
			newStates[i] = new Composite(container,SWT.NULL);
			GridLayout innerLayout = new GridLayout();
			innerLayout.numColumns = 2;
			newStates[i].setLayout(innerLayout);
				
			innerButton1[i] = new Button(newStates[i],SWT.RADIO);
			innerButton2[i] = new Button(newStates[i],SWT.RADIO);
			if(vm.getStatus().equals("running")){
				innerButton1[i].setText("Stop");
				innerButton2[i].setText("Terminate");
			}else{
				innerButton1[i].setText("Start");
				innerButton2[i].setText("Terminate");
			}
			
			scriptChooserButton[i] = new Button(container,SWT.PUSH);
			scriptChooserButton[i].setText("Click to choose");
			
			
			
			scriptChooserButton[i].addSelectionListener(new SelectionAdapter() 
			{
				  private int i = tempI;
				
				  public void widgetSelected(SelectionEvent e) {
				  FileDialog dialog = new FileDialog(getShell(), SWT.NULL);
				  String path = dialog.open();
				  jobScripts[i] = path;
				  scriptChooserButton[i].setText(path.substring(path.lastIndexOf("/")+1));
				  System.out.println(path);
				  }
			});
			
			presentStates[i].setEnabled(false);
			newStates[i].setVisible(false);
			innerButton1[i].setGrayed(false);
			innerButton2[i].setGrayed(false);
			scriptChooserButton[i].setEnabled(false);
			jobScripts[i]=null;
		}
		setControl(container);
		setPageComplete(true);
	}

}
