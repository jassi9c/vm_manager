package vm_manager.wizards;

import net.sf.json.JSONObject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import prometheus.Cloud;
import prometheus.RemoteExecutionRequest;
import prometheus.RemoteExecutionResponse;
import prometheus.RunScriptOnInstance;
import prometheus.VirtualMachine;
import vm_manager.views.Request;
import vm_manager.views.VMView;

public class StartStopTerminateVMWizard extends Wizard implements INewWizard{

	private Cloud cloud;
	private String sshKeysFolder;
	private SelectVmsStartStopTerminateWizardPage vmsPage;
	
	public StartStopTerminateVMWizard(Cloud cloud){
		super();
		this.cloud=cloud;
		setNeedsProgressMonitor(true);
		sshKeysFolder=vm_manager.Activator.getDefault().getPreferenceStore().getString("sshFolderPreference")+"/";
	}
	
	public void addPages(){
		vmsPage = new SelectVmsStartStopTerminateWizardPage(cloud);
		addPage(vmsPage);
	}
	
	@Override
	public boolean performFinish() {
		
		Button buttons[] = vmsPage.selectionButtons;
		for(int i=0;i<buttons.length;i++)
			if(buttons[i].getSelection()){
				String scriptPath = vmsPage.jobScripts[i];
				if(vmsPage.innerButton1[i].getSelection())
					performTask(buttons[i].getText(),scriptPath,vmsPage.innerButton1[i].getText());
				else
					performTask(buttons[i].getText(),scriptPath,vmsPage.innerButton2[i].getText());
			}
		
		return true;
	}
	
	private void performTask(final String hostname , final String preJobScript ,final String newState){
		VirtualMachine vm = cloud.getVirtualMachine(hostname);
		final JSONObject details = new JSONObject();
		details.put("hostname", hostname);
		details.put("username", "ec2-user");
		if(vm.getKeyName()==null){
			Display.getDefault().syncExec(new Runnable() {
				public void run() {
					//System.out.println(scriptRes.getOutput());
					MessageDialog.openInformation(getShell(), "Response", "Key not set for this vm :"+hostname);
				}
			});
			return;
		}
		details.put("sshKeyPath", sshKeysFolder+vm.getKeyName()+".pem");
		if(preJobScript!=null){
			details.put("sourceFile", preJobScript);
			details.put("targetFile", "~/"+preJobScript.substring(preJobScript.lastIndexOf("/")+1));
		}
		try{
			String jobDesc = null;
			if(newState.equals("Stop"))
				jobDesc = "Stopping ";
			else if(newState.equals("Start"))
				jobDesc = "Starting ";
			else
				jobDesc = "Terminating ";
			
			Job job = new Job("Running operations on : "+hostname){
				Request vm_ac_req = new Request();
				Request ori_req = null;
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					RemoteExecutionResponse scriptRes = null;
					RemoteExecutionResponse scriptRes2 =null;
					try{
						monitor.beginTask("Executing operations", 2);
						vm_ac_req.hostname=hostname;
						vm_ac_req.type=newState+" request";
						vm_ac_req.jobStatus="Running";
						vm_ac_req.request=details;
						vm_manager.views.VMActivity.addRequest(vm_ac_req);
						ori_req = vm_ac_req;
						if(preJobScript!=null && (newState.equals("Stop")||newState.equals("Terminate")) ){
							monitor.subTask("Running script before executing the "+newState+"request");
							System.out.println("running script");
							RunScriptOnInstance runScript = new RunScriptOnInstance(cloud, details);
							
							
							
							scriptRes = runScript.execute();
							monitor.worked(1);
							if(scriptRes.getReturnStatus()!=0){
								// to do
							}
							
							
							/*
							Display.getDefault().syncExec(new Runnable() {
								public void run() {
									System.out.println(scriptRes.getOutput());
									MessageDialog.openInformation(getShell(), "Response", scriptRes.getOutput());
								}
							});
							*/
							
						}
						monitor.subTask("Executing the "+newState+" request");
						if(newState.equals("Stop"))
							cloud.stopVM(hostname);
						else if(newState.equals("Start"))
							cloud.startVM(hostname);
						else
							cloud.terminateVM(hostname);
						monitor.worked(1);
						if(preJobScript!=null && newState.equals("Start")){
							monitor.subTask("Running script for the start request");
							System.out.println("running script");
							RunScriptOnInstance runScript = new RunScriptOnInstance(cloud, details);
							scriptRes2 = runScript.execute();
							monitor.worked(1);
							if(scriptRes2.getReturnStatus()!=0){
								// to do
							}
							/*
							Display.getDefault().syncExec(new Runnable() {
								public void run() {
									System.out.println(scriptRes2.getOutput());
									MessageDialog.openInformation(getShell(), "Response", scriptRes2.getOutput());
								}
							});
							*/
							
						}
						vm_ac_req.jobStatus="Finished";
						if(scriptRes!=null){
						vm_ac_req.exitStatus=scriptRes.getReturnStatus();
						vm_ac_req.output=scriptRes.getOutput();
						}
						else if(scriptRes2!=null){
							vm_ac_req.exitStatus=scriptRes.getReturnStatus();
							vm_ac_req.output=scriptRes.getOutput();
						}else{
							vm_ac_req.exitStatus=0;
							vm_ac_req.output="";
						}
						vm_manager.views.VMActivity.updateRequest(ori_req, vm_ac_req);
					}catch(Exception e){
						e.printStackTrace();
						System.out.println(e.getMessage());
					}finally{
						Display.getDefault().syncExec(new Runnable() {
							public void run() {
								VMView view = vm_manager.views.VMView.getDefault();
								if(view!=null)
									view.refresh();
							}
						});
					}
					
					return Status.OK_STATUS;
				}
			};
			job.schedule();
		}catch(Exception e){
			
		}
	}
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {		
	}

}
