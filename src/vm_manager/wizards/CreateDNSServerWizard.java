package vm_manager.wizards;

import java.io.File;
import java.io.FileInputStream;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.IJobChangeListener;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

import prometheus.Cloud;
import prometheus.LaunchInstances;
import prometheus.VirtualMachine;
import vm_manager.views.Request;
import vm_manager.views.VMView;

public class CreateDNSServerWizard extends Wizard implements INewWizard{

	protected InputBasicsWizardPage basicsPage;
	
	private Cloud cloud;
	
	public CreateDNSServerWizard(Cloud cloud) {
		super();
		this.cloud=cloud;
		setNeedsProgressMonitor(true);	
	}
	
	public void addPages() {
		basicsPage = new InputBasicsWizardPage("Set the basic parameters for the new dns server",cloud,false);
		addPage(basicsPage);
	}
	
	
	
	
	@Override
	public boolean performFinish() {
		
		final JSONObject details = new JSONObject();
		
		details.put("hostname", basicsPage.hostname.getText());
		details.put("ami", basicsPage.amiIDLabel.getText());
		details.put("securityGroup", basicsPage.securityGroupCombo.getText());
		details.put("sshKey",basicsPage.sshKeyCombo.getText());
		details.put("username", basicsPage.username.getText());
		
		String sshKeyPath = vm_manager.Activator.getDefault().getPreferenceStore().getString("sshFolderPreference")+"/"+basicsPage.sshKeyCombo.getText()+".pem";
		
		try{
			FileInputStream is = new FileInputStream(new File( sshKeyPath).getCanonicalFile());
		}catch(Exception e){
			MessageDialog.openWarning(getShell(), "Warning", "The ssh key ( "+basicsPage.sshKeyCombo.getText()+".pem ) could not be found in the ssh keys folder");
			return false;
		}
		
		details.put("sshKeyPath" , sshKeyPath);
		details.put("type", "t1.micro");
		details.put("isDNSServer", "Yes");
	
		JSONArray dnsServer = new JSONArray();
		dnsServer.add(basicsPage.hostname.getText());
		dnsServer.add("default");
		details.put("overrideDNS", dnsServer);
		
		try {
			final String hostname = basicsPage.hostname.getText();
			Job job = new Job("Creating DNS Server : "+hostname){
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					try {
						LaunchInstances req = new LaunchInstances(details, cloud);
						Thread temp = new Thread(req);
						monitor.beginTask("Executing the create dns server request", req.getTotalNumberOfStatus()-4);
						String lastStatus = req.getStatus();
						monitor.subTask(lastStatus);
						
						Request vm_ac_req = new Request();
						vm_ac_req.hostname=hostname;
						vm_ac_req.type="Create DNS Server";
						vm_ac_req.jobStatus="Running";
						vm_ac_req.request=details;
						vm_manager.views.VMActivity.addRequest(vm_ac_req);
						Request ori_req = vm_ac_req;

						temp.start();
						while(temp.isAlive()){
							for(int i=0;i<req.getTotalNumberOfStatus()-4;i++){
								while(req.getStatus().equals(lastStatus)){
									Thread.sleep(100);
									if(!temp.isAlive())
										break;
									if(monitor.isCanceled()){
										monitor.setCanceled(true);
										temp.stop();
										vm_ac_req.jobStatus="Cancelled";
										vm_manager.views.VMActivity.updateRequest(ori_req, vm_ac_req);
										this.done(Status.CANCEL_STATUS);
									}
								}
								if(!temp.isAlive())
									break;
								lastStatus=req.getStatus();
								monitor.subTask(lastStatus);
								monitor.worked(1);
								if(req.getStatus().equals("Finished."))
									break;
								if(monitor.isCanceled()){
									monitor.setCanceled(true);
									temp.stop();
									vm_ac_req.jobStatus="Cancelled";
									vm_manager.views.VMActivity.updateRequest(ori_req, vm_ac_req);
									this.done(Status.CANCEL_STATUS);
								}

							}
						}
						monitor.done();
						
						vm_ac_req.jobStatus="Finished";
						vm_ac_req.exitStatus=req.getResult().getReturnStatus();
						vm_ac_req.output=req.getResult().getOutput();
						vm_manager.views.VMActivity.updateRequest(ori_req, vm_ac_req);
						
						Display.getDefault().syncExec(new Runnable() {
							public void run() {
								VMView view = vm_manager.views.VMView.getDefault();
								if(view!=null)
									view.refresh();
							}
						});
						this.done(Status.OK_STATUS);
					} catch (Exception e) {
						e.printStackTrace();
						Display.getDefault().syncExec(new Runnable() {
							public void run() {
								VMView view = vm_manager.views.VMView.getDefault();
								if(view!=null)
									view.refresh();
							}
						});
						this.done(Status.CANCEL_STATUS);
					}
					return Status.OK_STATUS;
				}
				
			};
			
			job.addJobChangeListener(new IJobChangeListener() {
				@Override
				public void done(IJobChangeEvent event) {
		        if (event.getResult().isOK())
		        	System.out.println("Job completed successfully");
		        else{
		        	Display.getDefault().syncExec(new Runnable() {
						public void run() {
							MessageDialog.openWarning(getShell(), "Warning", "The vm might have been launched.\nTerminate it from the amazon web console.");
						}
					});
		        	System.out.println("Job did not complete successfully");
		        }
		        }
				@Override
				public void aboutToRun(IJobChangeEvent event) {
				}
				@Override
				public void awake(IJobChangeEvent event) {
				}
				@Override
				public void running(IJobChangeEvent event) {
				}
				@Override
				public void scheduled(IJobChangeEvent event) {
				}
				@Override
				public void sleeping(IJobChangeEvent event) {
				}
		     });
			//job.setUser(true);
			job.schedule();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {		
	}
	

}
