package vm_manager.wizards;

import net.sf.json.JSONObject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import prometheus.Cloud;
import prometheus.RemoteExecutionResponse;
import prometheus.VirtualMachine;
import vm_manager.views.Request;

public class DeployJarOnVmWizard extends Wizard implements INewWizard{
	
	private Cloud cloud;
	private String filePath;
	
	private ChooseVmAndUploadLocationWizardPage vmsAndLocations;
	
	public DeployJarOnVmWizard(Cloud cloud , String filePath){
		super();
		this.cloud=cloud;
		setNeedsProgressMonitor(true);
		this.filePath=filePath;
	}
	
	public void addPages() {
		vmsAndLocations = new ChooseVmAndUploadLocationWizardPage(cloud);
		addPage(vmsAndLocations);
	}

	@Override
	public boolean performFinish() {
		for(Button button: vmsAndLocations.selectionButtons){
			if(button!=null && button.getSelection()){
				System.out.println(button.getText());
				deployFile(button.getText(),vmsAndLocations.locations[(int)button.getData("i")].getText());
			}
		}
		
		return true;
	}
	
	private void deployFile(final String hostname , String targetFile){
		VirtualMachine vm = cloud.getVirtualMachine(hostname);
		String sshKey = vm_manager.Activator.getDefault().getPreferenceStore().getString("sshFolderPreference")+"/"+vm.getKeyName()+".pem";
		
		final JSONObject details = new JSONObject();
		
		details.put("host", vm.getPublicIp());
		details.put("sshKeyPath", sshKey);
		details.put("username", "ec2-user");
		details.put("sourceFile", filePath);
		details.put("targetFile", targetFile);
		
		try{
			Job job = new Job("Deploying the jar/war on "+hostname){
				Request vm_ac_req = new Request();
				Request ori_req = null;
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					try{
						
						vm_ac_req.hostname=hostname;
						vm_ac_req.type="Deploy jar on vm";
						vm_ac_req.jobStatus="Running";
						vm_ac_req.request=details;
						vm_manager.views.VMActivity.addRequest(vm_ac_req);
						ori_req = vm_ac_req;
						final boolean response =   cloud.uploadFile(details);
						/*
						Display.getDefault().syncExec(new Runnable() {
							public void run() {
								MessageDialog.openInformation(getShell(), "Response", "Result : "+response);
							}
						});
						*/
						vm_ac_req.jobStatus="Finished";
						vm_ac_req.exitStatus=response?0:1;
						vm_ac_req.output=response?"Successful":"Could not deploy jar";
						vm_manager.views.VMActivity.updateRequest(ori_req, vm_ac_req);
						
					}catch(Exception e){
						vm_ac_req.jobStatus="Finished";
						vm_ac_req.exitStatus=1;
						vm_ac_req.output="Could not deploy jar";
						vm_manager.views.VMActivity.updateRequest(ori_req, vm_ac_req);
						
						System.out.println("Could not deploy the file");
						System.out.println(details.toString());
						e.printStackTrace();
						return Status.CANCEL_STATUS;
					}
					return Status.OK_STATUS;
				}
			};
			job.schedule();
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {		
	}
	
}
