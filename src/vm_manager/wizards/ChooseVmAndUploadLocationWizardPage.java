package vm_manager.wizards;

import java.util.Collection;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import prometheus.Cloud;
import prometheus.VirtualMachine;

public class ChooseVmAndUploadLocationWizardPage extends WizardPage{

	protected Cloud cloud;
	protected Composite container;

	protected Button[] selectionButtons;
	protected Text[] locations;
	
	public ChooseVmAndUploadLocationWizardPage(Cloud cloud) {
		super("Choose vms to deploy the jar/war on.");
		setTitle("Deploy Jar/War on VM(s)");
		setDescription("Select vm(s) and input upload locations.");
		this.cloud=cloud;
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		container.setLayout(layout);
		
		Collection<VirtualMachine> vmHostnames = cloud.getAllVirtualMachines();
		Object[] vms = vmHostnames.toArray();

		int size = vmHostnames.size();
		selectionButtons = new Button[size];
		locations = new Text[size];
		
		String labels[] = new String[] {
				"Tick to select" , 
				"Upload to"
		};
		
		for(String option : labels){
			Label label = new Label(container,SWT.NONE);
			label.setText(option);
			
		}
		
		for(int i=0;i<size;i++){
			
			final int tempI = i;
			VirtualMachine vm = (VirtualMachine)vms[i];

			if(!vm.getStatus().equals("running"))
				continue;
			
			selectionButtons[i]= new Button(container, SWT.CHECK);
			selectionButtons[i].setText(vm.getHostname());
			selectionButtons[i].setData("i", i);
			
			selectionButtons[i].addSelectionListener(new SelectionAdapter(){
				private int i = tempI;
				public void widgetSelected(SelectionEvent e) {
					  Button button = (Button)e.getSource();
					  boolean state = button.getSelection();
					  locations[i].setEnabled(state);
				}
			});
			
			locations[i] = new Text(container,SWT.BORDER);
			locations[i].setEnabled(false);
			GridData gd = new GridData(GridData.FILL_HORIZONTAL);
			locations[i].setLayoutData(gd);
		}

		setControl(container);
		setPageComplete(true);

	}
	
}
