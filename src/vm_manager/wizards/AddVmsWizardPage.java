package vm_manager.wizards;

import java.util.ArrayList;

import org.apache.http.conn.DnsResolver;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import prometheus.Cloud;

public class AddVmsWizardPage extends WizardPage{
	
	protected Composite container;
	private Cloud cloud;
	
	protected Composite vmContainer;
	protected Composite messageContainer;
	
	/*
	protected ArrayList<Integer> seq;
	protected ArrayList<Text> hostnames;
	protected ArrayList<Text> amis;
	protected ArrayList<Text> keys;
	protected ArrayList<Text> groups;
	protected ArrayList<Text> scripts;
	protected ArrayList<Text> dnsServers;
	*/
	
	protected Label seq[];
	protected Label hostnames[];
	protected Label amis[];
	protected Label keys[];
	protected Label groups[];
	protected Label scripts[];
	protected Label dnsServers[];
	
	final private int upper_limit = 10;
	
	protected int num_reqs = 0;
	
	public AddVmsWizardPage(Cloud cloud){
		super("Launch Virtual Machines Wizard");
		setTitle("Specify details");
		setDescription("Specify details of the new vm(s) to be launched");
		this.cloud=cloud;
	}
	
	@Override
	public void createControl(Composite parent) {
	
		container = new Composite(parent, SWT.NULL);
		GridLayout outerlayout = new GridLayout();
		container.setLayout(outerlayout);
		outerlayout.numColumns = 1;
		
		messageContainer = new Composite(container, SWT.NULL);
		GridLayout messagelayout = new GridLayout();
		messageContainer.setLayout(messagelayout);
		messagelayout.numColumns = 1;
		
		vmContainer = new Composite(container, SWT.NULL);
		GridLayout vmlayout = new GridLayout();
		vmContainer.setLayout(vmlayout);
		vmlayout.numColumns = 7;
		
		
		
		
		/*
		seq = new ArrayList<Integer>();
		hostnames = new ArrayList<Text>();
		amis = new ArrayList<Text>();
		keys = new ArrayList<Text>();
		groups = new ArrayList<Text>();
		scripts = new ArrayList<Text>();
		dnsServers = new ArrayList<Text>();
		*/
		
		seq = new Label[upper_limit];
		hostnames = new Label[upper_limit];
		amis = new Label[upper_limit];
		keys = new Label[upper_limit];
		groups = new Label[upper_limit];
		scripts = new Label[upper_limit];
		dnsServers = new Label[upper_limit];
		
		String labels[] = {
			"Sequence" , 
			"Hostname" , 
			"AMI" ,
			"Ssh Key Name" ,
			"Security Group" ,
			"Startup Script" , 
			"DNS Server"
			//"Move up/down"
		};
		
		
		for(String label : labels){
			
			Label tempL = new Label(vmContainer,SWT.CENTER);
			tempL.setText(" "+label+" ");
			
			/*
			Composite tempC = new Composite(vmContainer, SWT.NULL);
			GridLayout tempClayout = new GridLayout();
			tempC.setLayout(tempClayout);
			tempClayout.numColumns = 2;
			
			Label tempL = new Label(tempC,SWT.NONE);
			tempL.setText(label);
			Label sep = new Label(tempC , SWT.SEPARATOR | SWT.VERTICAL);
			sep.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			*/

			
		}
		
		Button addVmButton = new Button(messageContainer,SWT.PUSH);
		addVmButton.setText("Click to add vm launch configuration");

		for(int i=0;i<upper_limit;i++){
			seq[i] = new Label(vmContainer,SWT.CENTER);
			seq[i].setVisible(false);
			hostnames[i]=new Label(vmContainer,SWT.CENTER);
			hostnames[i].setVisible(false);
			hostnames[i].setText(Integer.toString(i));
			amis[i]=new Label(vmContainer,SWT.CENTER);
			amis[i].setVisible(false);
			keys[i]=new Label(vmContainer,SWT.CENTER);
			keys[i].setVisible(false);
			groups[i]=new Label(vmContainer,SWT.CENTER);
			groups[i].setVisible(false);
			scripts[i]=new Label(vmContainer,SWT.CENTER);
			scripts[i].setVisible(false);
			dnsServers[i]=new Label(vmContainer,SWT.CENTER);
			dnsServers[i].setVisible(false);
		}
		
		
		
		
		
		addVmButton.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e) {
				
				if(num_reqs==10){
					MessageDialog.openInformation(getShell(), "", "More than 10 can't be launched in a single request");
					return;	
				}
				
				InputBasicsWizard basics = new InputBasicsWizard(cloud);
				WizardDialog wizardDialog = new WizardDialog(getShell(), basics );
				
				if (wizardDialog.open() == Window.OK) {
					vmContainer.setVisible(true);
					
					seq[num_reqs].setText(Integer.toString(num_reqs+1));
					seq[num_reqs].setVisible(true);
					
					hostnames[num_reqs].setText(basics.hostname);
					hostnames[num_reqs].setVisible(true);
					
					amis[num_reqs].setText(basics.ami);
					amis[num_reqs].setVisible(true);
					
					keys[num_reqs].setText(basics.key);
					keys[num_reqs].setVisible(true);
					
					groups[num_reqs].setText(basics.securityGroup);
					groups[num_reqs].setVisible(true);
					
					scripts[num_reqs].setText(basics.script==null?"None":basics.script);
					scripts[num_reqs].setVisible(true);
					
					dnsServers[num_reqs].setText(basics.dnsServer);
					dnsServers[num_reqs].setVisible(true);
					
					vmContainer.layout();
					container.layout();
					
					num_reqs++;
					
					if(num_reqs>0)
						setPageComplete(true);
					else
						setPageComplete(false);
				} 
				else {
				}

			  }
		});
		
		vmContainer.setVisible(false);
		
		setControl(container);
		setPageComplete(false);
	}
}
