package vm_manager.wizards;

import java.io.File;
import java.io.FileInputStream;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import prometheus.Cloud;

public class InputBasicsWizard extends Wizard implements INewWizard{
	
	protected InputBasicsWizardPage basicsPage;
	private Cloud cloud;
	
	protected String hostname;
	protected String ami;
	protected String key;
	protected String securityGroup;
	protected String script;
	protected String dnsServer;

	public InputBasicsWizard(Cloud cloud){
		super();
		this.cloud=cloud;
		setNeedsProgressMonitor(false);
	}
	
	public void addPages() {
		basicsPage = new InputBasicsWizardPage("Set the basic parameters for the vm to be launched",cloud,true);
		addPage(basicsPage);
	}
	
	@Override
	public boolean performFinish() {
		
		String sshKeyPath = vm_manager.Activator.getDefault().getPreferenceStore().getString("sshFolderPreference")+"/"+basicsPage.sshKeyCombo.getText()+".pem";
		
		try{
			FileInputStream is = new FileInputStream(new File( sshKeyPath).getCanonicalFile());
		}catch(Exception e){
			MessageDialog.openWarning(getShell(), "Warning", "The ssh key ( "+basicsPage.sshKeyCombo.getText()+".pem ) could not be found in the ssh keys folder");
			return false;
		}
	
		
		hostname = basicsPage.hostname.getText();
		ami = basicsPage.amiIDText.getText();
		key = basicsPage.sshKeyCombo.getText();
		script = basicsPage.startupScript;
		securityGroup = basicsPage.securityGroupCombo.getText();
		dnsServer = basicsPage.dnsServersCombo.getText();
		return true;
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {		
	}
}
