package vm_manager.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import prometheus.Cloud;

public class InputScriptCommandWizardPage extends WizardPage{

	protected Cloud cloud;
	protected Composite container;
	
	protected boolean addOptionForCommand;
	protected boolean addOptionForScript;
	
	protected Text commandBox;
	protected Button runAsRootButton;
	
	protected Text timeout;
	
	protected InputScriptCommandWizardPage(Cloud cloud,boolean addOptionForScript , boolean addOptionForCommand) {
		super("Choose script / command");
		this.cloud=cloud;
		this.addOptionForCommand=addOptionForCommand;
		this.addOptionForScript=addOptionForScript;
		if(addOptionForCommand && addOptionForScript){
			setTitle("Choose script and command");
			setDescription("Choose a script and command to be executed on the selected vms");
		}
		else if(addOptionForCommand){
			setTitle("Choose command");
			setDescription("Choose the command to be executed on the selected vms");
		}else{
			setTitle("Choose script");
			setDescription("Choose the script to be executed on the selected vms");
		}
		
			
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		container.setLayout(layout);
		
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		
		if(addOptionForScript){
		
			//runAsRootButton = new Button(container,SWT.CHECK);
			//runAsRootButton.setText("Run script as root ?");
			
		}
		if(addOptionForCommand){
			Label commandLabel = new Label(container, SWT.NONE);
			commandLabel.setText("Command");
			commandBox = new Text(container,SWT.BORDER);
			commandBox.setLayoutData(gd);
			commandBox.setText("");
		}
		
		Label timeoutLabel = new Label(container,SWT.NONE);
		timeoutLabel.setText("Command timeout ( in secs ) ");
		
		timeout = new Text(container,SWT.BORDER);
		timeout.setLayoutData(gd);
		timeout.setText("10");
		
		setControl(container);
		setPageComplete(true);
	}

}
