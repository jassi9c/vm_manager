package vm_manager.wizards;

import net.sf.json.JSONObject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import prometheus.Cloud;
import prometheus.RemoteExecutionResponse;
import prometheus.VirtualMachine;
import vm_manager.views.Request;

public class BroadcastCommandWizard extends Wizard implements INewWizard{
	
	private Cloud cloud;
	private SelectVmsPage selectVmsPage;
	private InputScriptCommandWizardPage commandPage;
	
	
	public BroadcastCommandWizard(Cloud cloud){
		super();
		this.cloud=cloud;
		setNeedsProgressMonitor(true);	
	}

	public void addPages() {
		selectVmsPage = new SelectVmsPage("For execution of the command", cloud);
		addPage(selectVmsPage);
		commandPage = new InputScriptCommandWizardPage(cloud, false, true);
		addPage(commandPage);
	}
	
	@Override
	public boolean performFinish() {
		
		System.out.println("Ready to be executed");
		boolean vmsSelected = false;
		for(Button button: selectVmsPage.selectionButtons){
			if(button!=null && button.getSelection()){
				vmsSelected = true;
				System.out.println(button.getText());
				executeCommand(button.getText(),commandPage.commandBox.getText());
			}
		}
		return true;
	}

	private void executeCommand(final String hostname, String command){
		VirtualMachine vm = cloud.getVirtualMachine(hostname);
		String sshKey = vm_manager.Activator.getDefault().getPreferenceStore().getString("sshFolderPreference")+"/"+vm.getKeyName()+".pem";
		
		final JSONObject details = new JSONObject();
		
		details.put("host", vm.getPublicIp());
		details.put("sshKeyPath", sshKey);
		details.put("username", "ec2-user");
		details.put("command", command);
		
		try{
			Job job = new Job("Executing command on "+hostname){
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					try{
						Request vm_ac_req = new Request();
						vm_ac_req.hostname=hostname;
						vm_ac_req.type="Broadcast command";
						vm_ac_req.jobStatus="Running";
						vm_ac_req.request=details;
						vm_manager.views.VMActivity.addRequest(vm_ac_req);
						
						Request ori_req = vm_ac_req;
						final RemoteExecutionResponse response =   cloud.runRemoteCommand(details);
						vm_ac_req.jobStatus="Finished";
						vm_ac_req.exitStatus=response.getReturnStatus();
						vm_ac_req.output=response.getOutput();
						vm_manager.views.VMActivity.updateRequest(ori_req, vm_ac_req);
						/*
						Display.getDefault().syncExec(new Runnable() {
							public void run() {
								MessageDialog.openInformation(getShell(), "Response", response.getOutput());
							}
						});
						*/
					}catch(Exception e){
						System.out.println("Could not execute the command");
						System.out.println(details.toString());
						e.printStackTrace();
						return Status.CANCEL_STATUS;
					}
					return Status.OK_STATUS;
				}
			};
			job.schedule();
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		
	}


}
