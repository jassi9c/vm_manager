package vm_manager;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import prometheus.AmazonCloud;
import prometheus.Cloud;


public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "VM_Manager"; //$NON-NLS-1$
	private static Activator plugin;	
	protected static Cloud cloud ;
	private String errorMessage;

	public static Cloud getCloud(){
		return cloud;
	}
	
	public boolean connectToCloudUsingCredentialsFile(String credentialsFile){
		try {
			cloud = new AmazonCloud(credentialsFile);
			errorMessage=null;
			return true;
		} catch (NullPointerException e) {
			errorMessage = e.getMessage();
		} catch (Exception e) {
			errorMessage = e.getMessage();
		}
		return false;
	}
	
	public Activator() {
		org.eclipse.jface.preference.IPreferenceStore store = getPreferenceStore();
		if(store!=null){
			if(connectToCloudUsingCredentialsFile(store.getString(vm_manager.preferences.PreferenceConstants.CREDENTIALS_FILE_PATH))==false)
			cloud=null;
		}
	}

	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		if(cloud==null &&connectToCloudUsingCredentialsFile(getPreferenceStore().getString(vm_manager.preferences.PreferenceConstants.CREDENTIALS_FILE_PATH))==false)
			cloud=null;
		this.getPreferenceStore()
		.addPropertyChangeListener(new IPropertyChangeListener() {
			@Override
			public void propertyChange(org.eclipse.jface.util.PropertyChangeEvent event) {
				if (event.getProperty().equals(vm_manager.preferences.PreferenceConstants.CREDENTIALS_FILE_PATH)) {
					//System.out.println("trying to connect...");
					vm_manager.preferences.VMManagerPreferences.statusField.setStringValue("Connecting...");
					if(connectToCloudUsingCredentialsFile(event.getNewValue().toString())==false){
						vm_manager.preferences.VMManagerPreferences.statusField.setStringValue("Not connected.");
						MessageDialog.openInformation(PlatformUI.getWorkbench().getDisplay().getActiveShell(),"Error Message","Could not connect using specified file. The error message returned is \n"+errorMessage);
						vm_manager.preferences.VMManagerPreferences.statusField.setErrorMessage(null);
					}
					else{
						vm_manager.preferences.VMManagerPreferences.statusField.setStringValue("Connected");
					}
				}
			}
		});
		
	}
	
	public void stop(BundleContext context) throws Exception {
		//System.out.println("stop called");
		plugin = null;
		super.stop(context);
	}
	
	public static Activator getDefault() {
		return plugin;
	}
	
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
}
